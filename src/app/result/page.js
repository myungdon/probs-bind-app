'use client'
import { useSearchParams } from "next/navigation";

export default function Result() {
    const searchParams = useSearchParams()
    const name = searchParams.get('name')
    const age = searchParams.get('age')
    const gender = searchParams.get('gender')

    return (
        <div className="text-center">
            <h1 className="text-5xl mt-72">결과</h1>
            <div className="mt-20">
                <p>이름: {name}</p>
                <p>나이: {age}</p>
                <p>성별: {gender}</p>
            </div>
            <br/>
            <a
                href="http://localhost:3000/">
                <button className="mt-20">처음으로</button>
            </a>
        </div>
    )
}