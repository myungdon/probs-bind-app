'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question3() {
    const [gender, setGender] = useState('')
    const router = useRouter()
    const searchParams = useSearchParams()
    const name = searchParams.get('name')
    const age = searchParams.get('age')

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/result?name=${name}&age=${age}&gender=${gender}`)
    }

    return (
        <div className="text-center">
            <h1 className="text-5xl mt-72">성별</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    <select value={gender} onChange={(e) => setGender(e.target.value)}>
                        <option value="">선택하세요</option>
                        <option value="male">남성</option>
                        <option value="female">여성</option>
                    </select>
                </label>
                <br/>
                <button type="submit" className="mt-20">다음</button>
            </form>
</div>
    )
}