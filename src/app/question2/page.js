'use client'
import { useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";

export default function Question2() {
    const [age, setAge] = useState('')
    const router = useRouter()
    const searchParams = useSearchParams()
    const name = searchParams.get('name')

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question3?name=${name}&age=${age}`)
    }

    return (
        <div className="text-center">
            <h1 className="text-5xl mt-72">나이</h1>
            <form onSubmit={handleSubmit}>
                <label>
                    <input type="number" value={age} onChange={(e) => setAge(e.target.value)} className="mt-20"/>
                </label>
                <br/>
                <button className="mt-20">다음</button>
            </form>
        </div>
    )
}