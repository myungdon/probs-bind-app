'use client'
import { useState } from "react"
import { useRouter } from "next/navigation"

export default function Question1() {
    const [name, setName] = useState('')
    const router = useRouter()

    const handleSubmit = e => {
        e.preventDefault()
        router.push(`/question2?name=${name}`)
    }

 return (
     <div className="text-center">
         <h1 className="text-5xl mt-72">이름</h1>
         <form onSubmit={handleSubmit}>
             <label>
                 <input type="text" value={name} onChange={(e) => setName(e.target.value)} className="mt-20"/>
             </label>
             <br/>
             <button type="submit" className="mt-20">다음</button>
         </form>
     </div>
 )
}